const appStates = {
    onboarding: {
        place: 'onboarding - place',
        adjust: 'onboarding - adjust',
    },
    main: 'main',
}

let appState = appStates.onboarding.place;

function setAppState(state)
{
    appState = state;
    switch(state)
    {
        case appStates.onboarding.place:
            document.getElementById('onboardingPlace').style.visibility = 'visible';
            document.getElementById('onboardingAdjust').style.visibility = 'hidden';
            document.getElementById('main').style.visibility = 'hidden';

            document.getElementById('worldAnchor').setAttribute('visible', 'false');
            document.getElementById('transformMarker').setAttribute('visible', 'false');
            document.getElementById('sceneViewer').setAttribute('visible', 'false');
            break;
            
        case appStates.onboarding.adjust:
            document.getElementById('onboardingPlace').style.visibility = 'hidden';
            document.getElementById('onboardingAdjust').style.visibility = 'visible';
            document.getElementById('main').style.visibility = 'hidden';

            document.getElementById('worldAnchor').setAttribute('visible', 'true');
            document.getElementById('transformMarker').setAttribute('visible', 'true');
            document.getElementById('sceneViewer').setAttribute('visible', 'false');
            break;
            
        case appStates.main:
            document.getElementById('onboardingPlace').style.visibility = 'hidden';
            document.getElementById('onboardingAdjust').style.visibility = 'hidden';
            document.getElementById('main').style.visibility = 'visible';

            document.getElementById('worldAnchor').setAttribute('visible', 'true');
            document.getElementById('transformMarker').setAttribute('visible', 'false');
            document.getElementById('sceneViewer').setAttribute('visible', 'true');
            break;
    }
}
