let currentSceneIndex = 0;
const scenes = [
    "phoenixBirdScene",
    "testAnimationDisasterTornadoScene",
    "testCityLoopScene",
    "cafeScene",
    "stressTestScene"
]

function nextScene()
{
    document.getElementById('nextButton').emit('animateNextButton');

    const n = currentSceneIndex + 1;
    const m = scenes.length;
    const i = ((n % m) + m) % m;

    gotoScene(i);
}

function previousScene()
{
    document.getElementById('previousButton').emit('animatePreviousButton');

    const n = currentSceneIndex - 1;
    const m = scenes.length;
    const i = ((n % m) + m) % m;

    gotoScene(i);
}

function gotoScene(index)
{
    let scene = document.getElementById(scenes[currentSceneIndex]);
    if(scene != null)
        scene.setAttribute('visible', 'false');

    currentSceneIndex = index;

    scene = document.getElementById(scenes[currentSceneIndex]);
    if(scene != null)
        scene.setAttribute('visible', 'true');
}
