AFRAME.registerComponent('world-anchor', 
{
  init: function()
  {
    const ground = document.getElementById('ground');
    const anchor = document.getElementById('worldAnchor');
    
    ground.addEventListener('click', event =>
    {
      if (appState == appStates.onboarding.place)
      {
        const touchPoint = event.detail.intersection.point;
        anchor.setAttribute('position', touchPoint);
        anchor.emit('animateWorldAnchor');
        setAppState(appStates.onboarding.adjust);
      }
    })
  }
});