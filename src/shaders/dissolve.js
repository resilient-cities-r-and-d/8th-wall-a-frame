/* global AFRAME, THREE */

const dissolveSpeed = 0.01;
const dissolveBoundsMin = -0.5;
const dissolveBoundsMax = 4.0;
var dissolveVec = { x: 0.0, y: 1.0, z: 3.0 };
var fadeOut = true;
var texture;
var DissolvingState = 1; //0 = no dissolve, 1 = full dissolve, 2 = part dissolve

AFRAME.registerComponent("shader-dissolve", {
  schema: {
    color: { type: "color", is: "uniform", default: "red" },
    MainTex: { type: "map", is: "uniform" },
    DissolveTex: { type: "map", is: "uniform" },
    dissolve: { type: "vec3", is: "uniform" },
    DissolveMap: { type: "map", is: "uniform" },
    DissolvingState: { type: "int", is: "uniform" },
  },
  vertexShader: `
        varying vec3 worldPos;
        varying vec2 vUV;
        varying vec3 vNormal;

        void main() { 
          vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
          worldPos = worldPosition.xyz;
          vUV = uv;
          vNormal = normal;
          gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }`,
  fragmentShader: `
        uniform vec3 color;
        varying vec3 worldPos;

        varying vec2 vUV;
        varying vec3 vNormal;
        uniform sampler2D MainTex;
        uniform sampler2D DissolveTex;
        uniform vec3 dissolve;
        uniform sampler2D DissolveMap;
        uniform int DissolvingState;
    
        float axis;
        float dissolveStrength;
        float cutoff;

        float GetAlpha() {
          float alpha = 1.0;
          alpha *= texture2D(MainTex, vUV).a;
          return alpha;
			  }

        void main() {
          cutoff = 0.2;
          dissolveStrength = dissolve.z;
          axis = worldPos.y;

          vec4 tex = texture2D(MainTex, vUV);
          vec4 nTex = texture2D(DissolveTex, vUV);
          vec4 dMap = texture2D(DissolveMap, vUV);

          if (DissolvingState == 2 ){
            if (dMap.x <= 0.0 && axis - dissolve.y >= nTex.x / dissolveStrength) // Partly Dissolve
              discard;
          }
          else if (DissolvingState == 1 ){
            if (axis - dissolve.y >= nTex.x / dissolveStrength) // Fully Dissolve
              discard;
          }

          if ((GetAlpha() - cutoff) < 0.0)
            discard;    

          gl_FragColor = tex;
        }`,
  init: function() {
    this.el.addEventListener("model-loaded", () => {
      this.update();
    });
  },
  update: function() {
    const mesh = this.el.getObject3D("mesh");

    if (mesh) {
      mesh.traverse(function(node) {
        if (node.isMesh) {
          texture = node.material.map;
        }
      });
    }

    var disTexture = textureLoader(
      "https://cdn.glitch.com/36004c97-0e0b-4b4f-9e68-fa2f7642538d%2FD_23.jpg?v=1593083813474"
    );
    
    var disMap = textureLoader(
      "https://cdn.glitch.com/36004c97-0e0b-4b4f-9e68-fa2f7642538d%2Fimage1.jpg?v=1593425369796"
    );

    const uniforms = {
      color: { value: { x: 1.0, y: 0.0, z: 0.0 } },
      MainTex: { value: texture },
      DissolveTex: { value: disTexture },
      dissolve: { value: dissolveVec },
      DissolveMap: {value: disMap},
      DissolvingState: {value: DissolvingState},
    };

    const material = new THREE.ShaderMaterial({
      uniforms: uniforms,
      vertexShader: this.vertexShader,
      fragmentShader: this.fragmentShader,
      side: THREE.DoubleSide
    });

    if (mesh) {
      mesh.traverse(function(node) {
        if (node.isMesh) {
          node.material = material;
          node.material.map = texture;
          node.material.needsUpdate = true;
        }
      });
    }
  },
  tick: function() {
    Dissolving();
  }
});

function textureLoader(imageSrc) {
  var texture = new THREE.TextureLoader().load(imageSrc, function(map) {
    map.encoding = THREE.sRGBEncoding;
    map.anisotropy = 16;
    map.flipY = false;
    map.needsUpdate = true;
  });

  return texture;
}

function Dissolving() {
  switch (fadeOut) {
    case true:
      dissolveVec.y += dissolveSpeed;

      if (dissolveVec.y >= dissolveBoundsMax) {
        fadeOut = false;
      }
      break;
    case false:
      dissolveVec.y -= dissolveSpeed;

      if (dissolveVec.y <= dissolveBoundsMin) {
        fadeOut = true;
      }
      break;
  }
}
